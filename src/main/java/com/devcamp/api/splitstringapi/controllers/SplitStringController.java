package com.devcamp.api.splitstringapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringController {
    
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(value="inputstring", defaultValue = "Mot Hai Ba Bon") String input) {
        ArrayList<String> listString = new ArrayList<String>();
        String[] arrString = input.split(" ");
        for (String string : arrString) {
            listString.add(string);
        }
        return listString;
    }
}
